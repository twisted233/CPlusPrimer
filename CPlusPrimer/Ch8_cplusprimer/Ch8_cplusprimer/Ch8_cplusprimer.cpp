// Ch8_cplusprimer.cpp: 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include "标头.h"

using namespace std;
class Solution {
public:
	int removeDuplicates(vector<int>& nums) {
		for (vector<int>::iterator i = nums.begin(); i != nums.end(); ) {
			if (*i == *(i + 1)) {
				i = nums.erase(i);
			}
			else
				++i;
		}
		return 0;
	}
	void display( vector<int>& n) {
		for (vector<int>::iterator i = n.begin(); i != n.end(); ) {
			cout << *i <<endl;
			++i;
		}
	}
};

int main(){
	//char s[] = "hello";
	//print_calls(s, 3);
	//song();
	//iquote(1); iquote(8.4352); iquote("adsgh");
	//box x = { "Tom",43.13122,1342.312,2134.2145 };
	//display(x);
	//calV(x);
	//display(x);

	Solution s;
	vector<int> v = { 1,324,32,4,4,52,6,4 };
	s.display(v);
	s.removeDuplicates(v);
	s.display(v);
    return 0;
}

