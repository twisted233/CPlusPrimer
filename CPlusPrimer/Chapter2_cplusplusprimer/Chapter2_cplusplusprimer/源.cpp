#include "stdafx.h"
#include <iostream>
#include <cmath>
#include "Tempe.h"

//
//Temperature::Temperature(double &c, double &f) {
//	Celsius = c;
//	Fahrenheit = f;
//}

Temperature::~Temperature(){}

void Temperature::getvalue() {
	using std::cin;
	using std::cout;
	
	char s[20];
	cout << "Please input a value like 8F or 98C:";
	cin >> s;
	int i = 0;
	while (s[i] != '\0') {
		if (s[i] == 'f' || s[i] == 'F') {
			this->Fahrenheit = trans(s,i);
			this->Celsius = (this->Fahrenheit - 32.0) / 1.8;
			break;
		}
		else if (s[i] == 'C' || s[i] == 'c') {
			this->Celsius = trans(s, i);
			this->Fahrenheit = this->Celsius *1.8 + 32.0;
			break;
		}
		i++;	
	}
}

double Temperature::trans(const char s[],int L) {
	int i = 0;
	for (; i < L; i++) {
		if (s[i] == '.') {
			break;
		}
	}

	double x = 0;
	for (int j = 0; j < i; j++) {
		x += (s[j]-'0') * std::pow(10,(i - j-1));
	}
	for (int j = i+1; j < L; j++) {
		x += (s[j]-'0') / std::pow(10,(j - i));
	}
	return x;
}

void Temperature::dispvalue() {
	std::cout << "F: " << this->Fahrenheit << std::endl;
	std::cout << "C: " << this->Celsius << std::endl;
}